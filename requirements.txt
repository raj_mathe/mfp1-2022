pip>=22.3.1
wheel>=0.37.1

# jupyter
ipython>=8.3.0
jupyter>=1.0.0
widgetsnbextension>=3.6.1
# FIXME
# according to https://github.com/microsoft/vscode-jupyter/issues/8552
# need to use v 7.7.2 for now
# ipywidgets>=8.0.2
ipywidgets==7.7.2

# running
codetiming>=1.3.0

# testing + dev
coverage[toml]>=6.4
pytest>=7.2.0
pytest-asyncio>=0.18.3
pytest-cov>=3.0.0
pytest-lazy-fixture>=0.6.3
pytest-order>=1.0.1
testfixtures>=6.18.5

# misc
importlib>=1.0.4
authlib>=1.0.1
passlib>=1.7.4
psutil>=5.9.0
pathlib>=1.0.1
lorem>=0.1.1
safetywrap>=1.5.0
typing>=3.7.4.3
nptyping>=2.1.1

# config
python-dotenv>=0.20.0
jsonschema>=4.4.0
lazy-load>=0.8.2
pyyaml>=6.0
pydantic>=1.9.1
datamodel-code-generator>=0.12.0

# maths
fraction>=2.2.0
numpy>=1.22.4
matplotlib>=3.6.1

# tables, data frames
pandas>=1.4.2
tabulate>=0.8.10
# array-to-latex>=0.82 # <- has issues
qiskit[visualization]>=0.38.0
