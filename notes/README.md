# Handnotizen #

Diese Notizen sollen _nicht_ als Musterlösungen verstanden werden.

Während der ÜG interagieren wir mit dem Stoff + den Aufgaben.
Wir besprechen **Konzepte** und **Ansätze**.
Diese Schrift ist also eine Abbildung dieser Diskussionen und Präsentationen.
Teilweise werden sie nur für die Leute Sinn ergeben, die daran teilgenommen haben und eigene Notizen machen.
Die Dokumente sind nicht vollständig, da einiges während der Stunden im Raum und an der Tafel erörtert wird.
