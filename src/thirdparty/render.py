#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from IPython.display import Latex;
from IPython.display import display;
from IPython.display import display_png;
from IPython.display import display as display_text_ipython;
from IPython.display import display_latex as display_latex_ipython;
from IPython.display import display_markdown as display_md_ipython;
import ipywidgets as widgets;
# from array_to_latex import to_ltx as array_to_latex; # <- has issues
from qiskit.visualization import array_to_latex;

from functools import wraps;
from typing import Callable;
from typing import TypeVar;

from src.thirdparty.misc import dedent;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MODIFICATIONS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

T1 = TypeVar('T1');
T2 = TypeVar('T2');

def display_with_dedent(pre_process: Callable[[str], T1] = lambda x: x):
    '''
    Returns a decorator that modifies string -> string methods
    '''
    def dec(method: Callable[[T1], T2]) -> Callable[[str], T2]:
        '''
        Performs method but dedents first.
        '''
        @wraps(method)
        def wrapped_method(text: str) -> T2:
            return method(pre_process(dedent(text)));
        return wrapped_method;

    return dec;

display_latex = display_with_dedent(Latex)(display_latex_ipython);
display_text = display_with_dedent()(display_text_ipython);
display_markdown = display_with_dedent()(display_md_ipython);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'Latex',
    'array_to_latex',
    'display',
    'display_latex',
    'display_markdown',
    'display_png',
    'widgets',
];
