#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from fractions import Fraction;
from numpy import cos;
from numpy import exp;
from numpy import linalg;
from numpy import linspace;
from numpy import pi;
from numpy import sin;
from numpy import sqrt;
from typing import TypeVar;
import math;
import numpy as np;
import random;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MODIFICATIONS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# local usage only
T = TypeVar('T');

def sample(
    X: list[T],
    size: int = 1,
    replace: bool = True,
) -> list[T]:
    '''
    @inputs
    - `X` - a list
    - `size` <int> - desired sample size
    - `replace` <bool> - optional replacement

    @returns a sample from an uniformly distributed set.
    '''
    return np.random.choice(X, size=size, replace=replace).tolist();

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'Fraction',
    'cos',
    'exp',
    'linalg',
    'linspace',
    'math',
    'np',
    'pi',
    'random',
    'sample',
    'sin',
    'sqrt',
];
