#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from __future__ import annotations;

from src.thirdparty.code import *;
from src.thirdparty.types import *;
from src.thirdparty.maths import *;
from src.thirdparty.plots import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'display_signal',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONSTANTS / VARIABLES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def display_signal(
    u: np.ndarray,
    title: str,
    max_labels = 10,
):
    n = len(u);
    if n <= max_labels:
        x_axis = list(range(n));
    else:
        skip = int(n/max_labels);
        x_axis = [ k*skip for k in range(max_labels) ];

    fig, axs = mplot.subplots(1, 1, constrained_layout=True);
    mplot.title(title);
    mplot.xlabel('»Zeit« [=Indizes]');
    mplot.ylabel('Werte [=Einträge]');
    axs.stem(linspace(0,n-1,n), np.real(u), label='reeller Teil', linefmt='red');
    axs.stem(linspace(0,n-1,n) + 0.05, np.imag(u), label='imaginärer Teil', linefmt='lime');
    mplot.legend();
    axs.set_xticks(x_axis, labels=[ str(index) for index in x_axis]);
    mplot.show();
    return;
